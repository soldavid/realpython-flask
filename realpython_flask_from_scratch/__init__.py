from flask import Flask

from realpython_flask_from_scratch import pages


def create_app():
    app = Flask(__name__)
    app.register_blueprint(pages.bp)
    return app


def main():
    app = create_app()
    app.run(host="0.0.0.0", port=8000, debug=True)


if __name__ == "__main__":
    main()
    main()
